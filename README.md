# ukwalks

Single page web app that uses OS Open Spaces to enable a person to create and store walking routes.

This will use IndexedDB for storage - so it's entirely in the browser.

Using TypeScript - to compile - `tsc`

Back Story
----------

Ukwalks was originally knocked up back in 2011 or so, and used some ugly Javascript.

The process has begun to convert that to TypeScript, so some of the new development is nice, tidy, but there
is some of the older stuff in there that worked, but hasn't been cleaned up.  That's on the TODO list :)