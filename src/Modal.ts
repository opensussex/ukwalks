interface Config {
	title: string,
	buttonText: string,
	buttonFunction: Function,
	content: string,
	formElements: Array<any>,
}


export class Modal {
	config: Config;

	constructor(config: Config) {
		this.config = config;
		this.create();
	}

	create(): void {

		let thisModalObject = this;
		let overlay: Element = this.createOverlay(thisModalObject);
		let modalElement:Element  = this.createModalBox(thisModalObject);
		let html: string = `
			<h3>{{title}}</h3>
			<p>{{content}}</p>
		`;
		
		html = html.replace('{{title}}', this.config.title);
		html = html.replace('{{content}}', this.config.content);
		modalElement.innerHTML = html;

		modalElement.classList.add('modal');
		modalElement = this.addForm(thisModalObject, modalElement);
		let button = document.createElement("button");
		button.innerHTML = this.config.buttonText;
		button.addEventListener('click', function() {thisModalObject.buttonEvent(thisModalObject)});
		modalElement.appendChild(button);
		document.body.appendChild(overlay);
		document.body.appendChild(modalElement);
	}

	buttonEvent(thisModalObject: Modal): void {
		thisModalObject.config.buttonFunction();
		this.closeModal();
	}

	createModalBox(thisModalObject: Modal): Element {
		let modalBox = document.createElement("div");
		modalBox.setAttribute('id','openModal');
		modalBox.style.position = "fixed";
		modalBox.style.top = "50%";
		modalBox.style.left = "50%"
		modalBox.style.margin = "0 auto";
		modalBox.style.zIndex = "10001";
		modalBox.style.backgroundColor = "#fff";
		modalBox.style.transform = "translate(-50%, -50%)";
		modalBox.style.padding = "2em";
		return modalBox;
	}

	createOverlay(thisModalObject: Modal): Element {
		let overlay = document.createElement('div');
		overlay.setAttribute('id','overlay');
		overlay.style.position = "fixed";
		overlay.style.top = "0";
		overlay.style.left = "0";
		overlay.style.width = "100%";
		overlay.style.height = "100%";
		overlay.style.zIndex = "10000";
		overlay.style.backgroundColor = "rgba(0,0,0,0.5)";
		overlay.addEventListener("click", function(){thisModalObject.closeModal()});
		return overlay;
	}

	addForm(thisModalObject: Modal, modalElement: Element): Element {
		const formElements = this.config.formElements;
		formElements.forEach(function(element){
			let tempInput = document.createElement('input');
			tempInput.setAttribute('id', element.id);
			tempInput.setAttribute('name', element.name);
			tempInput.setAttribute('placeholder', element.placeholder);
			tempInput.style.display = "block";
			modalElement.appendChild(tempInput);
		});
		return modalElement;
	}

	closeModal(): void {
			let modalElement:Element = document.getElementById("openModal");
			let overlay:Element = document.getElementById("overlay");
			overlay.remove();
			modalElement.remove();
	}
}

