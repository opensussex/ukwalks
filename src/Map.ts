import { Modal } from "./Modal";
import {DataStore} from "./DataStore";

declare var OpenLayers: any;
declare var OpenSpace: any;

let isDrawing: boolean = false;
let osMap;
let pt;
let position;
let vectorLayer;
let lonlat;
let points;
let p1;
let p2;
let linPr = new Array();
let lineString;
let lineFeature;
let totalDistance: number = 0;
const styleRed = {strokeColor: "#FF0000", strokeOpacity: 0.7, strokeWidth: 4, pointRadius: 1, pointerEvents: "visiblePainted"};
let dataStore: DataStore;
let loadedWalkId: number = 0;

export function initMap(mapDivId: string) {


    let controls = [new OpenLayers.Control.Navigation(),
        new OpenLayers.Control.KeyboardDefaults(),
        new OpenSpace.Control.CopyrightCollection(),
        new OpenLayers.Control.ArgParser()
    ];
    osMap = new OpenSpace.Map(mapDivId,{controls:controls});
    position = new OpenSpace.Control.ControlPosition(OpenSpace.Control.ControlAnchor.ANCHOR_TOP_RIGHT);
    osMap.addControl(new OpenSpace.Control.LargeMapControl(), position);
    osMap.setCenter(new  OpenSpace.MapPoint(528050, 125600), 4);
    vectorLayer = new OpenLayers.Layer.Vector("Vector Layer"); 
    osMap.addLayer(vectorLayer);
    osMap.events.register("mousemove", osMap, function(e) { 
        pt = osMap.getLonLatFromViewPortPx(e.xy); 
        let gridProjection = new OpenSpace.GridProjection();
        lonlat = gridProjection.getLonLatFromMapPoint(pt);
    });
    
   

    dataStore = new DataStore();

    if(!dataStore.isDbAvailable()) {
        displayDbWarning();
    };

    document.getElementById(mapDivId).addEventListener("click",clicked);
    initNavigation();
    initListOfWalks();

}

function initNavigation() {
    document.getElementById("draw").addEventListener("click", drawClicked); 
    document.getElementById("undo").addEventListener("click", undoClicked);
    document.getElementById("save").addEventListener("click", saveClicked);
    document.getElementById("erase").addEventListener("click", eraseClicked);
    document.getElementById("delete").addEventListener("click", deleteClicked);
    document.getElementById("share").addEventListener("click", shareClicked);
    document.getElementById("load").addEventListener("click", loadClicked);
}

function initListOfWalks() {
    let walksBox = document.createElement('div');
    walksBox.setAttribute('id','walksBox');
    let walksBoxList = document.createElement('ul');
    dataStore.all().then(function(results){
        results.forEach(function(item){
            let walkItem = document.createElement('li');
            let walkLink = document.createElement('a');
            walkLink.innerHTML = item.title;
            walkLink.href = '#';
            walkLink.setAttribute('data-id', String(item.id));
            walkItem.appendChild(walkLink);
            walkItem.addEventListener("click", loadWalkClicked);
            walksBoxList.appendChild(walkItem);
        });
        walksBox.appendChild(walksBoxList);
        let walkDetails = document.createElement('div');
        walkDetails.setAttribute('id','walkDetails');
        walksBox.appendChild(walkDetails);
    });

    document.body.appendChild(walksBox);
}

function reloadListOfWalks() {
    const walksBox = document.getElementById('walksBox');
    document.body.removeChild(walksBox);
    initListOfWalks();
}

function drawClicked(event) {
    
    toggleDrawingMode();
    if(isDrawing){
        event.target.classList.add('active');
    } else {
        event.target.classList.remove('active');
    }
}

function undoClicked() {
    undoLine();
    distance();
}

function eraseClicked() {
    removeLines();
    linPr = new Array();
    loadedWalkId = 0;
}


function undoLine(){
    removeLines();
    totalDistance = 0;
    linPr.pop();
    points = [];
    for(var i=0;i<linPr.length;i++){
        points.push(linPr[i]);
        points.push(linPr[i+1]);
        lineString = new OpenLayers.Geometry.LineString(points); 
        lineFeature = new OpenLayers.Feature.Vector(lineString, null, styleRed);
        vectorLayer.addFeatures([lineFeature]);
        totalDistance += lineString.getLength();
        osMap.addLayer(vectorLayer);
    }
    p1 = linPr[linPr.length-1];
}

function removeLines(){
    for(var i=0; i<osMap.layers.length; i++){
        var tmp = osMap.layers[i];
        if(tmp.name == "Vector Layer"){
            tmp.destroyFeatures();
            osMap.removeLayer(tmp);
        }
    }
    p1='';
}


function clicked(){
    if(!isDrawing){return;}
    points = [];
    p2 = p1;
    
    p1 = new OpenLayers.Geometry.Point(pt.lon, pt.lat);
    linPr.push(p1);//stores the full route in this array

    if(p2){
        points.push(p1);
        points.push(p2);
        lineString = new OpenLayers.Geometry.LineString(points); 
        lineFeature = new OpenLayers.Feature.Vector(lineString, null, styleRed);
        totalDistance += lineString.getLength();
        vectorLayer.addFeatures([lineFeature]);
        osMap.addLayer(vectorLayer);
    }

    distance();
}

function toggleDrawingMode() : void {
    isDrawing = !isDrawing;
}

function saveClicked(event) {
    toggleDrawingMode();
    document.querySelector("#draw span").classList.remove('active');
    const config = {
        title: 'New Walk',
        buttonText: 'Save',
        content: 'Save your walk to your local browser storage.',
        formElements: [
                        {
                            name: 'title',
                            id:   'title',
                            placeholder: 'Title: eg. Lovely walk'
                        },
                        {
                            name: 'description',
                            id:   'description',
                            placeholder: 'Description: eg. over the hill and far away'   
                        }
        ],
        buttonFunction: function(){

            let title = (<HTMLInputElement>document.getElementById('title')).value;
            let description = (<HTMLInputElement>document.getElementById('description')).value; 
            let walkDistance = distance();
            dataStore.add({
                title : title,
                description : description,
                distanceKm : walkDistance.km,
                distanceMiles : walkDistance.miles,
                route : createRouteString(),
            }); 
            reloadListOfWalks();  
        }
    };
    const saveModal = new Modal(config);
}

function createRouteString() : string {
    let route : Array<any> = [];
    let routeString : string;

	for(var i=0;i<linPr.length;i++){
		route.push({
            x : linPr[i].x,
            y : linPr[i].y
        });
	}
    return JSON.stringify(route);
}

function distance() {
    let totalDistanceKm = totalDistance /1000;
    totalDistanceKm = Math.round(totalDistanceKm*Math.pow(10,2))/Math.pow(10,2);
    let totalDistanceMiles = totalDistanceKm * 0.621371192;
    totalDistanceMiles = Math.round(totalDistanceMiles*Math.pow(10,2))/Math.pow(10,2);
    return {
        km : totalDistanceKm,
        miles: totalDistanceMiles
    }
}

function displayDbWarning() {
    const config = {
        title: 'Local Storage not Available',
        buttonText: 'close',
        content: 'You will not be able to save your walks to you browsers local storage.',
        formElements: [],
        buttonFunction: function(){}
    };
    const dbWarning = new Modal(config);  
}

function loadWalkClicked(event) {
    event.preventDefault();
    const walkId = event.target.dataset['id'];
    loadedWalkId = walkId;
    loadWalk(Number(walkId));
}

function deleteClicked() {
    dataStore.delete(Number(loadedWalkId)).then(function(count){
        if(count > 0) {
            loadedWalkId = 0;
            removeLines();
            reloadListOfWalks(); 
        }
    });
    linPr = new Array();
}

function shareClicked() {

}

function loadClicked() {
    
}

function loadWalk(walkId : number) {
    removeLines();
    dataStore.get(walkId).then(function(walk){
        const route = JSON.parse(walk.route);
        let linePoints : Array<any> = [];
        osMap.setCenter(new OpenSpace.MapPoint(route[0].x,route[0].y), 7);
        
        route.forEach(function(item){
            linePoints.push(new OpenLayers.Geometry.Point(item.x, item.y));
        });
        for(var i=0;i<linePoints.length;i++){
            points = [];
            points.push(linePoints[i]);
            points.push(linePoints[i+1]);
            lineString = new OpenLayers.Geometry.LineString(points); 
            lineFeature = new OpenLayers.Feature.Vector(lineString, null, styleRed);
            vectorLayer.addFeatures([lineFeature]);
            osMap.addLayer(vectorLayer);
        }
        const walkDetails = document.getElementById('walkDetails');
        walkDetails.innerHTML = '<h5>'+walk.title+'</h5><p>' + walk.description + '<br>'+ walk.distanceKm +' km<br>'+walk.distanceMiles+' miles</p>';
    });
}
