import {WalksDatabase} from "./WalksDatabase";
import {IWalk} from "./WalksDatabase";

export class DataStore {

	db: WalksDatabase;
	constructor() {
		this.db = new WalksDatabase();
	}

	isDbAvailable(): boolean {
		return true;
	}

	add(walk : IWalk) : Promise<number> {
		return this.db.walks.add(walk);
	}

	all() :Promise<IWalk[]>{
		return this.db.walks.toArray();
	}

	get(id : number) : Promise<IWalk> {
		return this.db.walks.get(id);
	}

	async delete(id : number) {
		return await this.db.walks
        .where("id").equals(id)
		.delete();
	}

}