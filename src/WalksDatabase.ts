import Dexie from 'dexie';

export interface IWalk {
	id?: number;
    title?: string;
    description?: string;
	distanceKm?: number;
    distanceMiles?: number;
    route: string;
}

export class WalksDatabase extends Dexie{
    walks: Dexie.Table<IWalk,number>;

    constructor() {
        super("WalksDB");
        this.version(1).stores({
            walks: "++id,title,distanceKm,distanceMiles"
        });
	}
}