import { initMap } from "./Map";
import { Modal } from "./Modal";

class Main {
	constructor (mapDivId:string) {
		initMap(mapDivId);	
		this.initInfo();
	}

	initInfo() {
		document.getElementById("about").addEventListener("click", this.aboutClicked);
		document.getElementById("privacy").addEventListener("click", this.privacyClicked);
	}

	aboutClicked(event) {
		event.preventDefault();
		const config = {
			title: 'About',
			buttonText: 'Close',
			content: `
			<p>
				Uk Walks allows you to save walks to your browsers local storage. 
			</p>
			<p>
				We currently use the Ordnance Survey Open Spaces Maps, Which means any walker, and rambler
			will be familiar with the maps they view online are the same as their paper maps
			</p>
			<p>
				This app is currently in Public Beta and we will be developing new features, such as:
				</p>
				<ul>
					<li>
						Ability to Back Up Walks
					</li>
					<li>
						Offline mode
					</li>
					<li>
						Share walks with others
					</li>
					<li>
						Download route Data
					</li>
					<li>
						Alternative map tiles.
					</li>
				</ul>
				<p>
					Ukwalks was developed by <a href="http://www.opensussex.net" class="alt-link">Open Sussex</a>
				</p>
			
			`
			,
			formElements: [],
			buttonFunction: function(){  
			}
		};
		const AboutModal = new Modal(config);
	}

	privacyClicked(event) {
		event.preventDefault();
		const config = {
			title: 'Privacy',
			buttonText: 'Close',
			content: `
			<p>
				We take your Privacy VERY seriously.  As such, we do not use cookies or tracking codes.
			</p>
			`
			,
			formElements: [],
			buttonFunction: function(){  
			}
		};
		const privacyModal = new Modal(config);
	}
}

export = Main;