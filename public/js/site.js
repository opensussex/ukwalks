var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define("Modal", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Modal = (function () {
        function Modal(config) {
            this.config = config;
            this.create();
        }
        Modal.prototype.create = function () {
            var thisModalObject = this;
            var overlay = this.createOverlay(thisModalObject);
            var modalElement = this.createModalBox(thisModalObject);
            var html = "\n\t\t\t<h3>{{title}}</h3>\n\t\t\t<p>{{content}}</p>\n\t\t";
            html = html.replace('{{title}}', this.config.title);
            html = html.replace('{{content}}', this.config.content);
            modalElement.innerHTML = html;
            modalElement.classList.add('modal');
            modalElement = this.addForm(thisModalObject, modalElement);
            var button = document.createElement("button");
            button.innerHTML = this.config.buttonText;
            button.addEventListener('click', function () { thisModalObject.buttonEvent(thisModalObject); });
            modalElement.appendChild(button);
            document.body.appendChild(overlay);
            document.body.appendChild(modalElement);
        };
        Modal.prototype.buttonEvent = function (thisModalObject) {
            thisModalObject.config.buttonFunction();
            this.closeModal();
        };
        Modal.prototype.createModalBox = function (thisModalObject) {
            var modalBox = document.createElement("div");
            modalBox.setAttribute('id', 'openModal');
            modalBox.style.position = "fixed";
            modalBox.style.top = "50%";
            modalBox.style.left = "50%";
            modalBox.style.margin = "0 auto";
            modalBox.style.zIndex = "10001";
            modalBox.style.backgroundColor = "#fff";
            modalBox.style.transform = "translate(-50%, -50%)";
            modalBox.style.padding = "2em";
            return modalBox;
        };
        Modal.prototype.createOverlay = function (thisModalObject) {
            var overlay = document.createElement('div');
            overlay.setAttribute('id', 'overlay');
            overlay.style.position = "fixed";
            overlay.style.top = "0";
            overlay.style.left = "0";
            overlay.style.width = "100%";
            overlay.style.height = "100%";
            overlay.style.zIndex = "10000";
            overlay.style.backgroundColor = "rgba(0,0,0,0.5)";
            overlay.addEventListener("click", function () { thisModalObject.closeModal(); });
            return overlay;
        };
        Modal.prototype.addForm = function (thisModalObject, modalElement) {
            var formElements = this.config.formElements;
            formElements.forEach(function (element) {
                var tempInput = document.createElement('input');
                tempInput.setAttribute('id', element.id);
                tempInput.setAttribute('name', element.name);
                tempInput.setAttribute('placeholder', element.placeholder);
                tempInput.style.display = "block";
                modalElement.appendChild(tempInput);
            });
            return modalElement;
        };
        Modal.prototype.closeModal = function () {
            var modalElement = document.getElementById("openModal");
            var overlay = document.getElementById("overlay");
            overlay.remove();
            modalElement.remove();
        };
        return Modal;
    }());
    exports.Modal = Modal;
});
define("WalksDatabase", ["require", "exports", "dexie"], function (require, exports, dexie_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var WalksDatabase = (function (_super) {
        __extends(WalksDatabase, _super);
        function WalksDatabase() {
            var _this = _super.call(this, "WalksDB") || this;
            _this.version(1).stores({
                walks: "++id,title,distanceKm,distanceMiles"
            });
            return _this;
        }
        return WalksDatabase;
    }(dexie_1.default));
    exports.WalksDatabase = WalksDatabase;
});
define("DataStore", ["require", "exports", "WalksDatabase"], function (require, exports, WalksDatabase_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DataStore = (function () {
        function DataStore() {
            this.db = new WalksDatabase_1.WalksDatabase();
        }
        DataStore.prototype.isDbAvailable = function () {
            return true;
        };
        DataStore.prototype.add = function (walk) {
            return this.db.walks.add(walk);
        };
        DataStore.prototype.all = function () {
            return this.db.walks.toArray();
        };
        DataStore.prototype.get = function (id) {
            return this.db.walks.get(id);
        };
        DataStore.prototype.delete = function (id) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4, this.db.walks
                                .where("id").equals(id)
                                .delete()];
                        case 1: return [2, _a.sent()];
                    }
                });
            });
        };
        return DataStore;
    }());
    exports.DataStore = DataStore;
});
define("Map", ["require", "exports", "Modal", "DataStore"], function (require, exports, Modal_1, DataStore_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var isDrawing = false;
    var osMap;
    var pt;
    var position;
    var vectorLayer;
    var lonlat;
    var points;
    var p1;
    var p2;
    var linPr = new Array();
    var lineString;
    var lineFeature;
    var totalDistance = 0;
    var styleRed = { strokeColor: "#FF0000", strokeOpacity: 0.7, strokeWidth: 4, pointRadius: 1, pointerEvents: "visiblePainted" };
    var dataStore;
    var loadedWalkId = 0;
    function initMap(mapDivId) {
        var controls = [new OpenLayers.Control.Navigation(),
            new OpenLayers.Control.KeyboardDefaults(),
            new OpenSpace.Control.CopyrightCollection(),
            new OpenLayers.Control.ArgParser()
        ];
        osMap = new OpenSpace.Map(mapDivId, { controls: controls });
        position = new OpenSpace.Control.ControlPosition(OpenSpace.Control.ControlAnchor.ANCHOR_TOP_RIGHT);
        osMap.addControl(new OpenSpace.Control.LargeMapControl(), position);
        osMap.setCenter(new OpenSpace.MapPoint(528050, 125600), 4);
        vectorLayer = new OpenLayers.Layer.Vector("Vector Layer");
        osMap.addLayer(vectorLayer);
        osMap.events.register("mousemove", osMap, function (e) {
            pt = osMap.getLonLatFromViewPortPx(e.xy);
            var gridProjection = new OpenSpace.GridProjection();
            lonlat = gridProjection.getLonLatFromMapPoint(pt);
        });
        dataStore = new DataStore_1.DataStore();
        if (!dataStore.isDbAvailable()) {
            displayDbWarning();
        }
        ;
        document.getElementById(mapDivId).addEventListener("click", clicked);
        initNavigation();
        initListOfWalks();
    }
    exports.initMap = initMap;
    function initNavigation() {
        document.getElementById("draw").addEventListener("click", drawClicked);
        document.getElementById("undo").addEventListener("click", undoClicked);
        document.getElementById("save").addEventListener("click", saveClicked);
        document.getElementById("erase").addEventListener("click", eraseClicked);
        document.getElementById("delete").addEventListener("click", deleteClicked);
        document.getElementById("share").addEventListener("click", shareClicked);
        document.getElementById("load").addEventListener("click", loadClicked);
    }
    function initListOfWalks() {
        var walksBox = document.createElement('div');
        walksBox.setAttribute('id', 'walksBox');
        var walksBoxList = document.createElement('ul');
        dataStore.all().then(function (results) {
            results.forEach(function (item) {
                var walkItem = document.createElement('li');
                var walkLink = document.createElement('a');
                walkLink.innerHTML = item.title;
                walkLink.href = '#';
                walkLink.setAttribute('data-id', String(item.id));
                walkItem.appendChild(walkLink);
                walkItem.addEventListener("click", loadWalkClicked);
                walksBoxList.appendChild(walkItem);
            });
            walksBox.appendChild(walksBoxList);
            var walkDetails = document.createElement('div');
            walkDetails.setAttribute('id', 'walkDetails');
            walksBox.appendChild(walkDetails);
        });
        document.body.appendChild(walksBox);
    }
    function reloadListOfWalks() {
        var walksBox = document.getElementById('walksBox');
        document.body.removeChild(walksBox);
        initListOfWalks();
    }
    function drawClicked(event) {
        toggleDrawingMode();
        if (isDrawing) {
            event.target.classList.add('active');
        }
        else {
            event.target.classList.remove('active');
        }
    }
    function undoClicked() {
        undoLine();
        distance();
    }
    function eraseClicked() {
        removeLines();
        linPr = new Array();
        loadedWalkId = 0;
    }
    function undoLine() {
        removeLines();
        totalDistance = 0;
        linPr.pop();
        points = [];
        for (var i = 0; i < linPr.length; i++) {
            points.push(linPr[i]);
            points.push(linPr[i + 1]);
            lineString = new OpenLayers.Geometry.LineString(points);
            lineFeature = new OpenLayers.Feature.Vector(lineString, null, styleRed);
            vectorLayer.addFeatures([lineFeature]);
            totalDistance += lineString.getLength();
            osMap.addLayer(vectorLayer);
        }
        p1 = linPr[linPr.length - 1];
    }
    function removeLines() {
        for (var i = 0; i < osMap.layers.length; i++) {
            var tmp = osMap.layers[i];
            if (tmp.name == "Vector Layer") {
                tmp.destroyFeatures();
                osMap.removeLayer(tmp);
            }
        }
        p1 = '';
    }
    function clicked() {
        if (!isDrawing) {
            return;
        }
        points = [];
        p2 = p1;
        p1 = new OpenLayers.Geometry.Point(pt.lon, pt.lat);
        linPr.push(p1);
        if (p2) {
            points.push(p1);
            points.push(p2);
            lineString = new OpenLayers.Geometry.LineString(points);
            lineFeature = new OpenLayers.Feature.Vector(lineString, null, styleRed);
            totalDistance += lineString.getLength();
            vectorLayer.addFeatures([lineFeature]);
            osMap.addLayer(vectorLayer);
        }
        distance();
    }
    function toggleDrawingMode() {
        isDrawing = !isDrawing;
    }
    function saveClicked(event) {
        toggleDrawingMode();
        document.querySelector("#draw span").classList.remove('active');
        var config = {
            title: 'New Walk',
            buttonText: 'Save',
            content: 'Save your walk to your local browser storage.',
            formElements: [
                {
                    name: 'title',
                    id: 'title',
                    placeholder: 'Title: eg. Lovely walk'
                },
                {
                    name: 'description',
                    id: 'description',
                    placeholder: 'Description: eg. over the hill and far away'
                }
            ],
            buttonFunction: function () {
                var title = document.getElementById('title').value;
                var description = document.getElementById('description').value;
                var walkDistance = distance();
                dataStore.add({
                    title: title,
                    description: description,
                    distanceKm: walkDistance.km,
                    distanceMiles: walkDistance.miles,
                    route: createRouteString(),
                });
                reloadListOfWalks();
            }
        };
        var saveModal = new Modal_1.Modal(config);
    }
    function createRouteString() {
        var route = [];
        var routeString;
        for (var i = 0; i < linPr.length; i++) {
            route.push({
                x: linPr[i].x,
                y: linPr[i].y
            });
        }
        return JSON.stringify(route);
    }
    function distance() {
        var totalDistanceKm = totalDistance / 1000;
        totalDistanceKm = Math.round(totalDistanceKm * Math.pow(10, 2)) / Math.pow(10, 2);
        var totalDistanceMiles = totalDistanceKm * 0.621371192;
        totalDistanceMiles = Math.round(totalDistanceMiles * Math.pow(10, 2)) / Math.pow(10, 2);
        return {
            km: totalDistanceKm,
            miles: totalDistanceMiles
        };
    }
    function displayDbWarning() {
        var config = {
            title: 'Local Storage not Available',
            buttonText: 'close',
            content: 'You will not be able to save your walks to you browsers local storage.',
            formElements: [],
            buttonFunction: function () { }
        };
        var dbWarning = new Modal_1.Modal(config);
    }
    function loadWalkClicked(event) {
        event.preventDefault();
        var walkId = event.target.dataset['id'];
        loadedWalkId = walkId;
        loadWalk(Number(walkId));
    }
    function deleteClicked() {
        dataStore.delete(Number(loadedWalkId)).then(function (count) {
            if (count > 0) {
                loadedWalkId = 0;
                removeLines();
                reloadListOfWalks();
            }
        });
        linPr = new Array();
    }
    function shareClicked() {
    }
    function loadClicked() {
    }
    function loadWalk(walkId) {
        removeLines();
        dataStore.get(walkId).then(function (walk) {
            var route = JSON.parse(walk.route);
            var linePoints = [];
            osMap.setCenter(new OpenSpace.MapPoint(route[0].x, route[0].y), 7);
            route.forEach(function (item) {
                linePoints.push(new OpenLayers.Geometry.Point(item.x, item.y));
            });
            for (var i = 0; i < linePoints.length; i++) {
                points = [];
                points.push(linePoints[i]);
                points.push(linePoints[i + 1]);
                lineString = new OpenLayers.Geometry.LineString(points);
                lineFeature = new OpenLayers.Feature.Vector(lineString, null, styleRed);
                vectorLayer.addFeatures([lineFeature]);
                osMap.addLayer(vectorLayer);
            }
            var walkDetails = document.getElementById('walkDetails');
            walkDetails.innerHTML = '<h5>' + walk.title + '</h5><p>' + walk.description + '<br>' + walk.distanceKm + ' km<br>' + walk.distanceMiles + ' miles</p>';
        });
    }
});
define("Main", ["require", "exports", "Map", "Modal"], function (require, exports, Map_1, Modal_2) {
    "use strict";
    var Main = (function () {
        function Main(mapDivId) {
            Map_1.initMap(mapDivId);
            this.initInfo();
        }
        Main.prototype.initInfo = function () {
            document.getElementById("about").addEventListener("click", this.aboutClicked);
            document.getElementById("privacy").addEventListener("click", this.privacyClicked);
        };
        Main.prototype.aboutClicked = function (event) {
            event.preventDefault();
            var config = {
                title: 'About',
                buttonText: 'Close',
                content: "\n\t\t\t<p>\n\t\t\t\tUk Walks allows you to save walks to your browsers local storage. \n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tWe currently use the Ordnance Survey Open Spaces Maps, Which means any walker, and rambler\n\t\t\twill be familiar with the maps they view online are the same as their paper maps\n\t\t\t</p>\n\t\t\t<p>\n\t\t\t\tThis app is currently in Public Beta and we will be developing new features, such as:\n\t\t\t\t</p>\n\t\t\t\t<ul>\n\t\t\t\t\t<li>\n\t\t\t\t\t\tAbility to Back Up Walks\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\tOffline mode\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\tShare walks with others\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\tDownload route Data\n\t\t\t\t\t</li>\n\t\t\t\t\t<li>\n\t\t\t\t\t\tAlternative map tiles.\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<p>\n\t\t\t\t\tUkwalks was developed by <a href=\"http://www.opensussex.net\" class=\"alt-link\">Open Sussex</a>\n\t\t\t\t</p>\n\t\t\t\n\t\t\t",
                formElements: [],
                buttonFunction: function () {
                }
            };
            var AboutModal = new Modal_2.Modal(config);
        };
        Main.prototype.privacyClicked = function (event) {
            event.preventDefault();
            var config = {
                title: 'Privacy',
                buttonText: 'Close',
                content: "\n\t\t\t<p>\n\t\t\t\tWe take your Privacy VERY seriously.  As such, we do not use cookies or tracking codes.\n\t\t\t</p>\n\t\t\t",
                formElements: [],
                buttonFunction: function () {
                }
            };
            var privacyModal = new Modal_2.Modal(config);
        };
        return Main;
    }());
    return Main;
});
//# sourceMappingURL=site.js.map